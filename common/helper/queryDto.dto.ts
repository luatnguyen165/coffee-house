import { Transform } from 'class-transformer';
import { IsDate, IsNumber, IsOptional, IsString } from 'class-validator';
import * as _ from 'lodash';
import { toNumber, toDate } from './cast.helper'

export class QueryDto {
  @Transform(({ value }) => _.toString(value))
  @IsString()
  @IsOptional()
  public search?: string;

  @Transform(({ value }) => toNumber(value, { default: 1, min: 1 }))
  @IsNumber()
  @IsOptional()
  public page?: number = 1;


  @Transform(({ value }) => toNumber(value,{ default: 10}))
  @IsNumber()
  @IsOptional()
  public limit?: number = 10;

  @Transform(({ value }) => toNumber(value, { default: -1}))
  @IsNumber()
  @IsOptional()
  public sort?: number = -1;

  @Transform(({ value }) => toDate(value))
  @IsDate()
  @IsOptional()
  public from?: Date;

  @Transform(({ value }) => toDate(value))
  @IsDate()
  @IsOptional()
  public to?: Date;
}