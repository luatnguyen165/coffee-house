

export interface createCollectionInterface {
    code: number;
    message: string;
}
export interface findAllCollectionInterface {
    code: number;
    message: string;
    data?: Array<Type>;
}
export interface Type {
    id?: number;
    title?: string;
    image?: string;
}