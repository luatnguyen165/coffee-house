

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CollectionDocument = Collection & Document;

@Schema()
export class Collection {
  @Prop({
    required: true
  })
  title: string;

  
  @Prop()
  image: string;
  
  @Prop({
    default: new Date()
  })
  createdAt?: Date;

  @Prop()
  updatedAt?: Date;
}

export const CollectionSchema = SchemaFactory.createForClass(Collection);