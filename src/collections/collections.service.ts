import { Injectable, Render } from '@nestjs/common';
import { CreateCollectionDto } from './dto/collections.dto';
import { Collection, CollectionDocument } from './schema/collection.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as _ from 'lodash';
import { QueryDto } from 'common/helper/queryDto.dto';
@Injectable()
export class CollectionsService {
  constructor(
    @InjectModel(Collection.name)
    private readonly model: Model<CollectionDocument>,
  ) {}

  async findAll({ search, limit, page, from, to, sort }: QueryDto) {
    try {
      const obj = {};
      if (!_.isEmpty(search)) {
        _.set(obj, 'title', {
          $regex: _.trim(search),
          $options: 'i',
        });
      }
      if (!_.isEmpty(from)) {
        _.set(obj, '$gte.createdAt', from);
      }
      if (!_.isEmpty(to)) {
        _.set(obj, '$lte.createdAt', to);
      }
      const listCollection = await this.model.find(obj).sort({
        createdAt: sort,
      });
      if (!listCollection) {
        return {
          code: 1001,
          message: 'get all collection failed',
        };
      }
      return {
        code: 1000,
        message: 'Get all collection successfully',
        total: _.toNumber(listCollection.length),
        page: _.toNumber(page),
        limit: _.toNumber(limit),
        data: listCollection.slice((page - 1) * limit, page * limit),
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  async findOne(id: number) {
    try {
      const obj = await this.model
        .findOne({
          CollectionId: id,
        })
        .exec();
      if (!obj) {
        return {
          code: 1001,
          message: "can't find collection",
        };
      }
      return {
        code: 1000,
        message: 'get collection successfully',
        data: obj,
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  async create(createCollectionDto: CreateCollectionDto) {
    try {
      const findObj = await this.model.findOne({
        title: {
          $regex: _.trim(_.toLower(createCollectionDto.title)),
          $options: 'i',
        },
      });
      if (findObj) {
        return {
          code: 1001,
          message: 'the name of collection is exists',
        };
      }
      const result = await new this.model({
        ...createCollectionDto,
        title: _.toLower(createCollectionDto.title),
        createdAt: new Date(),
      }).save();
      if (!result) {
        return {
          code: 1001,
          message: 'Collection created failed',
        };
      }
      return {
        code: 1000,
        message: 'Collection created successfully',
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  async update(UpdateCollectionDto, id: number) {
    try {
      const data = { ...UpdateCollectionDto, updatedAt: new Date() };
      const obj = await this.model.updateOne(
        {
          CollectionId: id,
        },
        { $set: data },
      );

      if (!obj) {
        return {
          code: 1001,
          message: 'Update collection failed',
        };
      }
      return {
        code: 1000,
        message: 'Update collection successfully',
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  async delete(id: number) {
    try {
      const obj = await this.model
        .findOneAndDelete({
          CollectionId: id,
        })
        .exec();
      if (!obj) {
        return {
          code: 1001,
          message: 'Delete collection failed',
        };
      }
      return {
        code: 1000,
        message: 'Delete collection successfully',
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }
}
