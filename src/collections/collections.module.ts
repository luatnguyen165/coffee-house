import { Module } from '@nestjs/common';
import { CollectionsService } from './collections.service';
import { CollectionsController } from './collections.controller';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Collection, CollectionSchema } from './schema/collection.schema';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from 'common/lib/multer.lib';
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Collection.name,
        useFactory: (connection: Connection) => {
          const AutoIncrement = AutoIncrementFactory(connection);
          const schema = CollectionSchema;
          schema.plugin(AutoIncrement, {
            inc_field: 'CollectionId',
            start_seq: 0
          });

          return schema;
        },
        inject: [getConnectionToken()]
      },
    ]),
    MulterModule.registerAsync({
      useFactory: () => ({
        storage: diskStorage({
          filename: editFileName,
          destination: './uploads'
        }),
        fileFilter: imageFileFilter
      }),
    })
  ],
  controllers: [CollectionsController],
  providers: [CollectionsService]
})
export class CollectionsModule {}
