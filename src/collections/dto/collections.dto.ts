import { Transform } from "class-transformer";
import { IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import * as _ from "lodash";
var slugify = require('slugify')

export class CreateCollectionDto {
    
    @IsString()
    @MaxLength(255)
    @MinLength(2)
    public title: string;

    @IsOptional()
    image: string;


}
export class UpdateCollectionDto {
    @IsOptional()
    @MaxLength(255)
    @MinLength(2)
    public title: string;
    
    @IsOptional()
    image: string;
}
