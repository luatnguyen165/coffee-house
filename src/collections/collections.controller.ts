import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  Render,
  UseInterceptors,
  UploadedFiles,
  UploadedFile,
  ParseFilePipe,
  Query,
  DefaultValuePipe,
  ValidationPipe,
  UsePipes,
  Session,
} from '@nestjs/common';
import {
  FileFieldsInterceptor,
  FileInterceptor,
  FilesInterceptor,
} from '@nestjs/platform-express';
import { CollectionsService } from './collections.service';
import {
  CreateCollectionDto,
  UpdateCollectionDto,
} from './dto/collections.dto';
import { Express } from 'express';
import * as _ from 'lodash';
import { QueryDto } from 'common/helper/queryDto.dto';
@Controller('collections')
export class CollectionsController {
  constructor(private readonly collectionsService: CollectionsService) {}

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(
    @UploadedFile() image: Express.Multer.File,
    @Body() createCollectionDto: CreateCollectionDto,
  ) {
    const obj = { ...createCollectionDto };
    if (_.get(image, 'path', false) !== false) {
      _.set(obj, 'image', _.get(image, 'path', ''));
    }
    return await this.collectionsService.create(obj);
  }

  // @Post('file')
  // @UseInterceptors(FileInterceptor('file'))
  // uploadFileAndPassValidation(
  //   @Body() body: {
  //     name: string
  //   },
  //   @UploadedFile()
  //   file: Express.Multer.File,
  // ) {
  //   return {
  //     body,
  //     file,
  //   };
  // }
  // @Post('upload')
  // @UseInterceptors(
  //   FileFieldsInterceptor([
  //   { name: 'avatar', maxCount: 1 },
  //   { name: 'background', maxCount: 1 },
  // ]))
  // uploadFile(@Body() name: string,@UploadedFiles() files: { avatar?: Express.Multer.File[], background?: Express.Multer.File[] }) {
  //   try {
  //     console.log(name);
  //     console.log(files);
  //   } catch (error) {
  //     console.log(error.message);
  //   }
  // }
  @Get()
  async findAll(
    @Session() session: Record<string, any>,
    @Query() query: QueryDto,
  ) {
    console.log(session.id);
    return await this.collectionsService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.collectionsService.findOne(+id);
  }
  @Put(':id')
  updateOne(
    @Body(new ValidationPipe()) updateCollectionDto: UpdateCollectionDto,
    @Param('id') id: string,
  ) {
    console.log('updateOne', updateCollectionDto);
    return this.collectionsService.update(updateCollectionDto, +id);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.collectionsService.delete(+id);
  }
}
