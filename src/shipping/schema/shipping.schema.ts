


import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ShippingDocument = Shipping & Document;

@Schema()
export class Shipping {
    @Prop({
        required: true
    })
    location: string;

    @Prop()
    rateName: string;

    @Prop()
    shippingFee: number;

    @Prop({
        default: 'USD'
    })
    currency: string;
    
    @Prop({
        default: false
    })
    condition: Boolean;
    @Prop()
    maxPrice: Number;

    @Prop()
    minPrice: Number;

    @Prop({
        default: new Date()
      })
    createdAt?: Date;

    @Prop({
        default: new Date()
      })
    updateAt?: Date;
}

export const ShippingSchema = SchemaFactory.createForClass(Shipping);