import { Transform } from "class-transformer";
import { IsBoolean, IsDate, IsNumber, IsOptional, IsString, MinLength } from "class-validator";
import { toUpperCase } from "common/helper/cast.helper";
import * as _ from "lodash";

export class CreateShippingDto {
    @IsString()
    @MinLength(2)
    location: string;


    @IsString()
    @MinLength(5)
    rateName: string;

    @IsNumber()
    shippingFee: number;

    @IsString()
    @IsOptional()
    @MinLength(3)
    @Transform((value)=>toUpperCase(_.toString(value)))
    currency: string;
    

    @IsBoolean()
    @IsOptional()
    condition: Boolean;

    @IsNumber()
    @IsOptional()
    maxPrice: Number;

    @IsNumber()
    @IsOptional()
    minPrice: Number;

  
    @IsDate()
    @IsOptional()
    createdAt?: Date;

    @IsDate()
    @IsOptional()
    updateAt?: Date;
}
