import { Module } from '@nestjs/common';
import { ShippingService } from './shipping.service';
import { ShippingController } from './shipping.controller';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { Shipping, ShippingSchema } from './schema/shipping.schema';
import * as AutoIncrementFactory from 'mongoose-sequence';
@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: Shipping.name,
        useFactory: (connection: Connection) => {
          const AutoIncrement = AutoIncrementFactory(connection);
          const schema = ShippingSchema;
          schema.plugin(AutoIncrement, {
            inc_field: 'ShippingId',
            start_seq: 0
          });

          return schema;
        },
        inject: [getConnectionToken()]
      },
    ]),
  ],
  controllers: [ShippingController],
  providers: [ShippingService]
})
export class ShippingModule {}

