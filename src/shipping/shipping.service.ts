import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { QueryDto } from 'common/helper/queryDto.dto';
import { Model } from 'mongoose';
import { CreateShippingDto } from './dto/create-shipping.dto';
import { UpdateShippingDto } from './dto/update-shipping.dto';
import { Shipping, ShippingDocument } from './schema/shipping.schema';
import *as _ from 'lodash';
@Injectable()
export class ShippingService {
  constructor(
    @InjectModel(Shipping.name) private readonly model: Model<ShippingDocument>,
  ) {}
  async create(createShippingDto: CreateShippingDto) {
    try {
      const findObj =  await this.model.findOne({
        rateName: { $regex: _.trim(_.toLower(createShippingDto.rateName)), $options: 'i'}  
      })
      if(findObj){
        return {
          code: 1001,
          message: "the name of Shipping is exists"
        }
      }
      const result = await new this.model({
        ...createShippingDto,
        rateName:_.toLower(createShippingDto.rateName),
        createdAt: new Date(),
      }).save();
      if(!result){
        return {
          code: 1001,
          message: "Shipping created failed"
        }
      }
      return {
        code: 1000,
        message: "Shipping created successfully"
      } 
     } catch (error) {
        return {
          code: 500,
          message: "Internal Server Error"
        }
     }
  }

  async findAll({search,from,to,page,limit,sort}: QueryDto) {
    try {
      let obj = {}
      if(!_.isEmpty(search)){
          let arraySearch = [
            { rateName: {
              $regex: _.trim(search), $options: 'i'
            }},
              { location: {
              $regex: _.trim(search), $options: 'i'
            }}
          ]
          _.set(obj, '$or',arraySearch)
      }
      if(!_.isEmpty(from)){
        _.set(obj, '$gte.createdAt',from)
      }
      if(!_.isEmpty(to)){
        _.set(obj, '$lte.createdAt',to)
      }   
      const listShipping =  await this.model.find(obj).sort({
        'createdAt': sort
      });
      if(!listShipping){
        return {
          code: 1001,
          message: "get all shipping failed"
        }
      }
      return {
        code: 1000,
        message: "Get all shipping successfully",
        total:  _.toNumber(listShipping.length),
        page: _.toNumber(page),
        limit: _.toNumber(limit),
        data: listShipping.slice((page-1)*limit,page*limit)
        }
    } catch (error) {
      console.log(error);
      
      return {
        code: 500,
        message: "Internal Server Error"
      }
      
    }
  }

  async findOne(id: number) {
    try {
      const obj = await this.model.findOne({
        ShippingId: id
      }).exec();
      if(!obj){
        return {
          code:1001,
          message:"can't find shipping"
        }
      }
      return {
        code: 1000,
        message: "get shipping successfully",
        data: obj
      }
     } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
     }
  }

  async update(id: number, updateShippingDto: UpdateShippingDto) {
    try {
      let data = {...updateShippingDto,updatedAt: new Date()}
      const obj = await this.model.updateOne({
        ShippingId : id
      },{$set: data})
      
      if(!obj){
        return {
          code: 1001,
          message: 'Update shipping failed'
        }
      }
      return {
        code: 1000,
        message: 'Update shipping successfully'
      }

    } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
    }
  }
  async remove(id: number) {
    try {
      const obj =  await this.model.findOneAndDelete({
        ShippingId: id
      }).exec();
      if(!obj){
        return {
          code: 1001,
          message: 'Delete shipping failed'
        }
      }
      return {
        code: 1000,
        message: 'Delete shipping successfully'
      }
     } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
     }
  }
}
