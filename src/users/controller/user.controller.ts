import { Controller, Get, UseGuards,Request } from '@nestjs/common';
import { currentUser } from 'src/auth/decorator/user.decorator';
import { Action } from 'src/casl/casl-ability.factory/casl-ability.factory';
import {
  CheckPolicies,
  ReadUserAbility,
} from 'src/casl/decorator/casl.decorator';
import { PoliciesGuard } from 'src/casl/guard/PoliciesGuard.guard';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';

@Controller('Users')
export class UserController {
  @UseGuards(PoliciesGuard)
  @CheckPolicies(new ReadUserAbility())
  @UseGuards(JwtAuthGuard)
  @Get('test')
  async test(@Request() req) {
    return req.user
  }
  @Get('test2')
  async testq() {
    return 'vào rồi nè 2'
  }

}
