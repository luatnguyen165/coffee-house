import { HttpException, HttpStatus, Injectable, OnModuleInit } from '@nestjs/common';

import { LoginDto, RegisterDto } from '../dto/user.dto';
import { User, UsersDocument } from '../schema/user.schema';
import { UserInterface } from './../interfaces/users.interface';
import * as bcrypt from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly model: Model<UsersDocument>,
  ) {}

  async findByEmail(email: string) {
    return await this.model.findOne({
     email
    });
  }
  async signIn({ email, password }: LoginDto) {
    try {
      const user = await this.findByEmail(email);
      if (!user)
        throw new HttpException('Email is not exist', HttpStatus.NOT_FOUND);

      const isMatch = await this._compareHash(password, user.password);
      if (!isMatch) {
        throw new HttpException('Invalid password', HttpStatus.BAD_REQUEST);
      }

      
      return user;
    } catch (error) {
      throw new HttpException('ERROR INTERNAL SERVER', 500);
    }
  }
  async signUp({ email, password, fullName }: RegisterDto) {
    try {
      const user = await this.findByEmail(email);
    
      
      if (user) {
        throw new HttpException('Email is exist', HttpStatus.FOUND);
      }
      const hash = await this._hash(password);
      const item = await new this.model({
        email,
        password: hash,
        fullName,
      }).save()
    
      return item;
    } catch (error) {
      console.log(error);
      
      throw new HttpException('ERROR INTERNAL SERVER', 500);
    }
  }
  private async _compareHash(payload, hash) {
    return await bcrypt.compare(payload, hash);
  }
  private async _hash(payload) {
    const salt = await bcrypt.genSalt();
    return await bcrypt.hash(payload, salt);
  }
}
