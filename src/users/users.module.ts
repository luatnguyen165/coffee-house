import { forwardRef, Module } from '@nestjs/common';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
// import { APP_GUARD } from '@nestjs/core';
// import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { CaslModule } from 'src/casl/casl.module';
// import { PoliciesGuard } from 'src/casl/guard/PoliciesGuard.guard';
import { UserController } from './controller/user.controller';
import { User, UserSchema } from './schema/user.schema';
import { UsersService } from './service/users.service';
import * as AutoIncrementFactory from 'mongoose-sequence';
import { AppModule } from 'src/app.module';
@Module({
  imports: [
      MongooseModule.forFeature([
        {
          name: User.name,
          schema: UserSchema
        }
      ])
    ,CaslModule],
  providers: [UsersService],
  exports: [UsersService],
  controllers: [UserController],
})
export class UsersModule {}
