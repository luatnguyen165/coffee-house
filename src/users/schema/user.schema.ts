import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UsersDocument = User & Document;

@Schema()
export class User {
  @Prop()
  fullName: string;
  
  @Prop()
  email: string;

  @Prop()
  address: string;

  @Prop()
  streetAddress: string;

  @Prop()
  zipcode: string;

  @Prop()
  state: string;

  @Prop()
  phone: string;

  @Prop()
  password: string;

  @Prop()
  isAdmin: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);