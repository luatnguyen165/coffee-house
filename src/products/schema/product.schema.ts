import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  @Prop({
    required: true,
  })
  title: string;

  @Prop()
  slug: string;

  @Prop()
  images: string[];

  @Prop()
  description: string;

  @Prop()
  price: number;

  @Prop()
  comparePrice: number;

  @Prop()
  costPer: number;

  @Prop()
  profit: number;

  @Prop()
  margin: number;

  @Prop()
  type: string;

  @Prop()
  attributes: [
    {
      size: string;
      sku: string;
      sizeImage: string;
      price: number;
      comparePrice: number;
      available: {
        type: string;
        default: 0
      };
    },
  ];


  @Prop({
    type: String,
    default: 'ACTIVE',
    enum: ['DRAFT', 'ACTIVE'],
  })
  status: string;
  @Prop({
    default: new Date(),
  })
  createdAt?: Date;

  @Prop()
  updatedAt?: Date;

  @Prop()
  tags: string[];

  @Prop()
  collections: string[];
}

export const ProductSchema = SchemaFactory.createForClass(Product);
