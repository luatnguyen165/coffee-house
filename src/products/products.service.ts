import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { QueryDto } from 'common/helper/queryDto.dto';
import { Model } from 'mongoose';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product, ProductDocument } from './schema/product.schema';
import * as _ from 'lodash';
const generateUniqueId = require('generate-unique-id');
@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private readonly model: Model<ProductDocument>,
  ) {}
  async create(createProductDto: any) {
    if (_.get(createProductDto, 'attributes', []).length > 0) {
      for (let i = 0; i < createProductDto.attributes.length; i++) {
        createProductDto.attributes[i].sku = `SKU-${generateUniqueId({
          length: 15,
          useNumbers: true,
          useLetters: false,
        })}`;
      }
    }
    const obj = await new this.model({
      ...createProductDto,
    }).save();
    if (!obj) {
      return {
        code: 1001,
        message: 'Add new product failed',
      };
    }
    return {
      code: 1000,
      message: 'Add new product successfully',
    };
  }

  async findAll({ search, limit, page, from, to, sort }: QueryDto) {
    try {
      const obj = {};
      if (!_.isEmpty(search)) {
        const arraySearch = [
          {
            title: {
              $regex: _.trim(search),
              $options: 'i',
            },
          },
          {
            slug: {
              $regex: _.trim(search),
              $options: 'i',
            },
          },
          {
            description: {
              $regex: _.trim(search),
              $options: 'i',
            },
          },
        ];
        _.set(obj, '$or', arraySearch);
      }
      if (!_.isEmpty(from)) {
        _.set(obj, '$gte.createdAt', from);
      }
      if (!_.isEmpty(to)) {
        _.set(obj, '$lte.createdAt', to);
      }
      const listProduct = await this.model
        .find(obj)
        .sort({
          createdAt: sort,
        })
        .select('-_id -__v');
      if (!listProduct) {
        return {
          code: 1001,
          message: 'get all products failed',
        };
      }
      return {
        code: 1000,
        message: 'Get all products successfully',
        total: _.toNumber(listProduct.length),
        page: _.toNumber(page),
        limit: _.toNumber(limit),
        data: listProduct.slice((page - 1) * limit, page * limit),
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  async findOne(id: number) {
    try {
      const obj = await this.model
        .findOne({
          ProductId: id,
        })
        .exec();
      if (!obj) {
        return {
          code: 1001,
          message: "can't find product",
        };
      }
      return {
        code: 1000,
        message: 'get product successfully',
        data: obj,
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  async remove(id: number) {
    try {
      const obj = await this.model
        .findOneAndDelete({
          ProductId: id,
        })
        .exec();
      if (!obj) {
        return {
          code: 1001,
          message: 'Delete product failed',
        };
      }
      return {
        code: 1000,
        message: 'Delete product successfully',
      };
    } catch (error) {
      return {
        code: 500,
        message: 'Internal Server Error',
      };
    }
  }
}
