import { Module } from '@nestjs/common';
import { CollectionsModule } from './collections/collections.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { OrderProductsModule } from './order-products/order-products.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ContactsModule } from './contacts/contacts.module';
import { PaypalModule } from './paypal/paypal.module';
import { ShippingModule } from './shipping/shipping.module';
import { CaslModule } from './casl/casl.module';
import { BullModule } from '@nestjs/bull';
import { DiscountsModule } from './discounts/discounts.module';
import * as _ from 'lodash';

@Module({
  imports: [
    ConfigModule.forRoot(),
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_QUEUE_HOST,
        port: _.toNumber(process.env.REDIS_QUEUE_PORT),
      },
    }),
    MongooseModule.forRoot(process.env.DATABASE_URI),
    CollectionsModule,
    ProductsModule,
    OrdersModule,
    OrderProductsModule,
    UsersModule,
    AuthModule,
    CaslModule,
    ContactsModule,
    PaypalModule,
    ShippingModule,
    DiscountsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
