


import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OrderProductDocument = OrderProduct & Document;

@Schema()
export class OrderProduct {
  @Prop()
  TransactionId: string;

  @Prop()
  productId: number;

  @Prop()
  sku: string;

  @Prop()
  price: number;

  @Prop()
  quantity: number;

}

export const OrderProductSchema = SchemaFactory.createForClass(OrderProduct);