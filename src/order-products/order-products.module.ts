import { Module } from '@nestjs/common';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { OrderProductsService } from './order-products.service';
import { OrderProduct, OrderProductSchema } from './schema/order-product.schema';
import * as AutoIncrementFactory from 'mongoose-sequence';
@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: OrderProduct.name,
        useFactory: (connection: Connection) => {
          const AutoIncrement = AutoIncrementFactory(connection);
          const schema = OrderProductSchema;
          schema.plugin(AutoIncrement, {
            inc_field: 'OrderProductId',
            start_seq: 0
          });

          return schema;
        },
        inject: [getConnectionToken()]
      },
    ]),
  ],
  providers: [OrderProductsService],
  exports: [OrderProductsService]
})
export class OrderProductsModule {}
