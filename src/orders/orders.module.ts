import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { BullModule } from '@nestjs/bull';
import { OrderProductsService } from 'src/order-products/order-products.service';
import { OrderProductsModule } from 'src/order-products/order-products.module';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'orderItems',
      redis: {
        port: 6379,
      },
    }),
    OrderProductsModule,
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
