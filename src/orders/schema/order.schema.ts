import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ContactDocument = Order & Document;

@Schema()
export class Order {
  @Prop()
  orderId: string;

  @Prop()
  userId: string;

  @Prop()
  discount: number;

  @Prop({
    default: new Date(),
  })
  orderDate: Date;

  @Prop()
  totalBefore: number;

  @Prop()
  total: number;

  @Prop({
    default: 'PAYPAL',
  })
  paymentBy: string;

  @Prop({
    type: String,
    default: 'IN PROGRESS',
    enum: ['SUCCEEDED', 'FAILED', 'CANCELLED'],
  })
  status: string;

  @Prop()
  shippingID: number;

  @Prop({
    default: 'United States',
  })
  location: string;

  @Prop({
    default: new Date(),
  })
  createdAt?: Date;
}

export const ContactSchema = SchemaFactory.createForClass(Order);
