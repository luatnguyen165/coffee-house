import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { OrderProductsService } from 'src/order-products/order-products.service';
import asyncForEach from 'async-await-foreach';
@Injectable()
export class OrdersService {
  constructor(@InjectQueue('orderItems') private orderItemQueue: Queue) {}

  create(createOrderDto: CreateOrderDto) {
    return 'This action adds a new order';
  }

  findAll() {
    return `This action returns all orders`;
  }

  findOne(id: number) {
    return `This action returns a #${id} order`;
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  remove(id: number) {
    return `This action removes a #${id} order`;
  }
  private async _orderBulkQueue(products, transactionId) {
    await asyncForEach(products, async (item) => {
      await this.orderItemQueue.add({
        ...item,
        transactionId,
      });
    });
  }
}
