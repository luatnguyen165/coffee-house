import { Controller, Get, Post, Body, Delete, Session } from '@nestjs/common';
import { OrdersService } from './orders.service';
const Redis = require('ioredis');
const redis = new Redis();
import * as _ from 'lodash';
@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Get('cart')
  async create(@Session() session: Record<string, any>) {
    const products = [];
    const productList = await redis.hgetall('cart:' + _.get(session, 'id', ''));
    if (_.isEmpty(productList)) {
      return {
        code: 1000,
        message: 'Giỏ hàng đang trống... ',
      };
    }
    for (const product of Object.keys(productList)) {
      products.push(JSON.parse(productList[product]));
    }
    return {
      code: 1000,
      data: products,
    };
  }

  @Post('add')
  async addCart(
    @Body() { productId, sku, price, amount }: any,
    @Session() session: Record<string, any>,
  ) {
    session = _.get(session, 'id', '');

    if (
      session === null ||
      sku == null ||
      amount == null ||
      price == null ||
      productId === null
    ) {
      return {
        code: 1001,
        message: 'Cart item details or session was not given',
      };
    }
    if (amount === 0) {
      return await redis.hdel('cart:' + session, _.toString(sku));
    }
    let productStore = await redis.hget('cart:' + session, _.toString(sku));

    if (productStore) {
      productStore = JSON.parse(productStore);
      productStore.amount += amount;
      await redis.hset(
        'cart:' + session,
        _.toString(sku),
        JSON.stringify(productStore),
      );
      return {
        code: 1000,
        message: 'Cập nhật số lượng giỏ hàng thành công',
      };
    }
    await redis.hset(
      'cart:' + session,
      _.toString(sku),
      JSON.stringify({ productId, sku, price, amount }),
    );
    await redis.expire('cart:' + session, 262800000);
    return {
      code: 1000,
      message: 'Thêm sản phẩm thành công!!!',
    };
  }
  @Delete('cart')
  async deleteProductCart(
    @Body() { sku }: any,
    @Session() session: Record<string, any>,
  ) {
    await redis.hdel('cart:' + session.id, _.toString(sku));
    return {
      code: 1000,
      message: 'Xoá sản phẩm trong giỏ hàng thành công',
    };
  }
  @Delete('allCart')
  async deleteAllCart(@Session() session: Record<string, any>) {
    await redis.expire('cart:' + session.id, 0);
    return {
      code: 1000,
      message: 'Xoá sản phẩm trong giỏ hàng thành công',
    };
  }
}
