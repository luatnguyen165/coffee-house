import { IsArray, IsString, IsNumber } from 'class-validator';

export class CreateOrderDto {
  orderId: string;
  carts: [
    {
      productId: string;
      sku: string;
      quantity: number;
    },
  ];
  shippingFee: number;
  discountFee: number;
  userId: string;
  totalBefore: number;
  total: number;
  shippingID: number;
  location: string;

}
