import { Injectable, OnModuleInit } from '@nestjs/common';
const paypal = require('paypal-rest-sdk');
import *as _ from 'lodash';
@Injectable()
export class PaypalService implements OnModuleInit {
  onModuleInit() {
    paypal.configure({
      mode: process.env.PAYPAL_ENV,
      client_id: process.env.PAYPAL_CLIENT_ID,
      client_secret: process.env.PAYPAL_CLIENT_SECRET_KEY
    });

  }

  async create(createPaypalDto:any,res) {
    try {
      const create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: process.env.PAYPAL_METHOD
        },
        redirect_urls: {
          return_url: process.env.PAYPAL_REDIRECT_SUCCESS_URL,
          cancel_url: process.env.PAYPAL_REDIRECT_FAILED_URL
        },
        transactions: [{
          item_list: {
            "items": [{
              "name": "Red Sox Hat",
              "sku": "001",
              "price": "25.00",
              "currency": "USD",
              "quantity": 1
          }]
          },
          amount: {
            currency: process.env.PAYPAL_CURRENCY,
            total: "25.00"
          },
          description: "Hat for the best team ever"
        }]
      };
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          throw error;
        } else {
          for (let i = 0; i < payment.links.length; i++) {
            if (payment.links[i].rel === 'approval_url') {
              res.redirect(payment.links[i].href);
            }
          }
        }
      });
    } catch (error) {
      return {
        code: 500,
        message: 'INTERNAL ERROR SERVER'
      }
    }
  }

  async paySuccess(req,res) {
    try {
      const payerId = req.query.PayerID;
      const paymentId = req.query.paymentId;
    
      // get data 
      const execute_payment_json = {
        "payer_id": payerId,
        "transactions": [{
            "amount": {
                "currency": "USD",
                "total": "25.00"
            }
        }]
      };
    
      paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log(JSON.stringify(payment));
            res.send('Success');
        }
    });
    } catch (error) {
      return {
        code: 500,
        message: 'INTERNAL ERROR SERVER'
      }
    }
  }

  async payCancel(res) {
    return res.send('cancel');
  }
}

