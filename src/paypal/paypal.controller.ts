import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Render, Req } from '@nestjs/common';
import { PaypalService } from './paypal.service';
import { Request ,Response} from 'express';
@Controller('paypal')
export class PaypalController {
  constructor(private readonly paypalService: PaypalService) {}

  @Get('pay')
  @Render('index')
  getPay() {
    try {
      return {
        message: 'ok'
      } ;
    } catch (error) {
      console.log(error);
      
    }
  }
  @Post('pay')
  create(@Body() createPaypalDto: any,@Res() res: Response) {
    return this.paypalService.create(createPaypalDto,res);
  }

  @Get('success')
  paySuccess(@Req() req: Request,@Res() res: Response) {
    return this.paypalService.paySuccess(req,res);
  }
  @Get('success')
  paySend(@Req() req: Request,@Res() res: Response) {
    return res.send('cancel')
  }

}
