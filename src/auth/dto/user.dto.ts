import { IsEmail, IsString } from "class-validator";

export class RegisterDto { 
    
    @IsString()
    @IsEmail()
    email: string;

    @IsString()
    password: string;

    @IsString()
    fullName: string;
}
export class LoginDto {
    @IsEmail()
    email: string;

    @IsString()
    password: string;
}
