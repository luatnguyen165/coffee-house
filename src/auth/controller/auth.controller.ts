import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import { LoginDto, RegisterDto } from '../dto/user.dto';
import { AuthService } from '../services/auth.service';
import { JwtAuthGuard } from '../guard/jwt-auth.guard';
import { currentUser } from '../decorator/user.decorator';
import { PoliciesGuard } from 'src/casl/guard/PoliciesGuard.guard';
import {
  CheckPolicies,
} from 'src/casl/decorator/casl.decorator';
import { AppAbility } from 'src/casl/casl-ability.factory/casl-ability.factory';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('signIn')
  async login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }
  @Post('SignUp')
  async signUp(@Body() register: RegisterDto) {
    return this.authService.register(register);
  }

}
