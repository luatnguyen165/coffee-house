import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from 'src/users/service/users.service';
import { JwtService } from '@nestjs/jwt';
import { LoginDto, RegisterDto } from '../dto/user.dto';
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}
  async register(register: RegisterDto) {
    try {
      const user = await this.usersService.signUp(register);
      if (!user)
        throw new HttpException('failed registered', HttpStatus.BAD_REQUEST);
      return {
        message: 'successfully registered',
        status: 1000,
      };
    } catch (error) {
      console.log(error);
    }
  }
  async login(loginDto: LoginDto) {
    const user = await this.usersService.signIn(loginDto);
    if (!user) throw new HttpException('Invalid login', HttpStatus.NOT_FOUND);
    const { password, ...result } = user;
    const token = await this._createToken(result);
    return {
      ...result,
      accesstoken: token,
    };
  }
  private async _createToken(payload) {
    try {
      return await this.jwtService.sign(payload);
    } catch (error) {
      console.log(error);
    }
  }
}
