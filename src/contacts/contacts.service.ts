import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateContactDto } from './dto/create-contact.dto';
import { Contact, ContactDocument } from './schema/contact.schema';
import { QueryDto } from './../../common/helper/queryDto.dto';
import *as _ from 'lodash';
import {Cache} from 'cache-manager';
var md5 = require('md5');
@Injectable()
export class ContactsService {
  constructor(
    @InjectModel(Contact.name) private readonly model: Model<ContactDocument>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {}
  async create(createContactDto: CreateContactDto) {
    try {
      // blocking ddos form send contact
      const key = md5(JSON.stringify({...createContactDto}))
      const getKey = await this.cacheManager.get(key);
      if(getKey){
        return {
          code: 1001,
          message: "your contact had been sent before can't send again"
        }
      }
      const obj =  await new this.model({
        ...createContactDto,
        createdAt: new Date(),
      }).save()
      if(!obj){
        return {
          code: 1001,
          message: "Send contact failed"
        }
      }
      await this.cacheManager.set(key,key,24*60*60*1000)
      return {
        code: 1000,
        message: "Send contact successfully"
      }
    } catch (error) {
      return {
        code: 500,
        error: 'INTERNER ERROR SERVER'
      }
    }
  }

  async findAll({search,from,to,page,limit,sort}: QueryDto) {
    try {
      let obj = {}
      if(!_.isEmpty(search)){
          _.set(obj, 'email',{
            $regex: _.trim(search), $options: 'i'
          })
      }
      if(!_.isEmpty(from)){
        _.set(obj, '$gte.createdAt',from)
      }
      if(!_.isEmpty(to)){
        _.set(obj, '$lte.createdAt',to)
      }   
      const listContact =  await this.model.find(obj).sort({
        'createdAt': sort
      });
      if(!listContact){
        return {
          code: 1001,
          message: "get all contact failed"
        }
      }
      return {
        code: 1000,
        message: "Get all contact successfully",
        total:  _.toNumber(listContact.length),
        page: _.toNumber(page),
        limit: _.toNumber(limit),
        data: listContact.slice((page-1)*limit,page*limit)
        }
    } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
      
    }
  }

  async findOne(id: number) {
    try {
      const obj = await this.model.findOne({
        ContactId: id
      }).exec();
      if(!obj){
        return {
          code:1001,
          message:"can't find contact"
        }
      }
      return {
        code: 1000,
        message: "get contact successfully",
        data: obj
      }
     } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
     }
  }
  async remove(id: number) {
    try {
      const obj =  await this.model.findOneAndDelete({
        ContactId: id
      }).exec();
      if(!obj){
        return {
          code: 1001,
          message: 'Delete contact failed'
        }
      }
      return {
        code: 1000,
        message: 'Delete contact successfully'
      }
     } catch (error) {
      return {
        code: 500,
        message: "Internal Server Error"
      }
     }
  }
}
