


import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ContactDocument = Contact & Document;

@Schema()
export class Contact {
    @Prop()
    fullName : string;

    @Prop()
    email: string;


    @Prop()
    phone: string;
    
    @Prop()
    venue: string;

    @Prop({
        default: new Date()
      })
    createdAt?: Date;
}

export const ContactSchema = SchemaFactory.createForClass(Contact);