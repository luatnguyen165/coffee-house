import { IsEmail, IsOptional, IsPhoneNumber, IsString } from "class-validator";

export class CreateContactDto {
    @IsString()
    fullName : string;

    @IsString()
    @IsEmail()
    email: string;


    @IsString()
    @IsPhoneNumber('AU')
    phone: string;
    
    @IsOptional()
    venue: string;
}
