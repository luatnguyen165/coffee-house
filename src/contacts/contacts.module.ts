import { CacheModule, Module } from '@nestjs/common';
import { ContactsService } from './contacts.service';
import { ContactsController } from './contacts.controller';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Contact, ContactSchema } from './schema/contact.schema';
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence';
@Module({
  imports: [
    CacheModule.register(),
    MongooseModule.forFeatureAsync([
      {
        name: Contact.name,
        useFactory: (connection: Connection) => {
          const AutoIncrement = AutoIncrementFactory(connection);
          const schema = ContactSchema;
          schema.plugin(AutoIncrement, {
            inc_field: 'ContactId',
            start_seq: 0
          });

          return schema;
        },
        inject: [getConnectionToken()]
      },
    ]),
  ],
  controllers: [ContactsController],
  providers: [ContactsService]
})
export class ContactsModule {}

