import {
    Ability,
    AbilityBuilder,
    AbilityClass,
    ExtractSubjectType,
    InferSubjects,
  } from '@casl/ability';
import { User } from 'src/users/schema/user.schema';
 
  
  export enum Action {
    Manage = 'manage',
    Create = 'create',
    Read = 'read',
    Update = 'update',
    Delete = 'delete',
  }
  export type Subjects = InferSubjects<typeof User> | 'all';
  export type AppAbility = Ability<[Action, Subjects]>;
  export class CaslAbilityFactory {
    createForUser(user: User) {
      const { can, cannot, build } = new AbilityBuilder<
        Ability<[Action, Subjects]>
      >(Ability as AbilityClass<AppAbility>);
        
      if (user.isAdmin) {
        can(Action.Manage, 'all'); // read-write access to everything
        
      } else {
        can(Action.Read, User); // read-only access to everything
      }
      // cannot(Action.Read, User).because('Only for admin')
      return build({
        detectSubjectType: (item) =>
          item.constructor as ExtractSubjectType<Subjects>,
      });
    }
  }
  