import { SetMetadata } from '@nestjs/common';
import {
  Action,
  AppAbility,
  Subjects,
} from '../casl-ability.factory/casl-ability.factory';
import { User } from 'src/users/schema/user.schema';

export interface PolicyHandler {
  action: Action;
  subject: Subjects;
}
export const CHECK_POLICIES_KEY = 'check_policy';
export const CheckPolicies = (...handlers: PolicyHandler[]) =>
  SetMetadata(CHECK_POLICIES_KEY, handlers);

export class ReadUserAbility implements PolicyHandler {
  action = Action.Read;
  subject = User;
}
